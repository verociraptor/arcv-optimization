#include "../../../include/ArcV/Processing/Image.hpp"
#include "../../../include/ArcV/Processing/Sobel.hpp"
#include "../../../include/ArcV/Math/Matrix.hpp"
#include <vector>
#include <chrono> //necessary to track time.
#include <fstream> //necessary to access streams.
#include <iostream> //necessary for output.
#include <stdlib.h>

#define MAXIMUM_CORES 4992  //Maximum cores CUDA can use.

#define MAXIMUM_THREADS 1024 //Maxmimu threads CUDA can generate.

__global__ void applyDetectorHelperGPU(const int SOBEL_WIDTH, const int SOBEL_HEIGHT, const int SOBEL_WIDTH_LOOP,
					const float* directionMat, float* resMat, const float* sobelMat);

namespace Arcv {

namespace Image {

template<>
Matrix<> applyDetector<ARCV_DETECTOR_TYPE_CANNY>(const Matrix<>& mat) {
  const Sobel sobel(applyFilter<ARCV_FILTER_TYPE_GAUSSIAN_BLUR>(changeColorspace<ARCV_COLORSPACE_GRAY>(mat)));
  const Matrix<> directionMat = sobel.computeGradientDirection();
  Matrix<> res = sobel.getSobelMat(); //Resolution of the sobel.

/************OUR CODE START**************************************/
  const int SOBEL_WIDTH = sobel.getSobelMat().getWidth();

  const int SOBEL_WIDTH_LOOP = SOBEL_WIDTH - 1;

  const int SOBEL_HEIGHT = sobel.getSobelMat().getHeight() - 1; 

  //Starts a timer to the time it takes to execute the nested for loop.
  
  const auto startTime = std::chrono::system_clock::now();

/*************OUR CODE ENDS************************************/
  for (std::size_t heightIndex = 1; heightIndex < SOBEL_HEIGHT; ++heightIndex) {
    for (std::size_t widthIndex = 1; widthIndex < SOBEL_WIDTH_LOOP; ++widthIndex) {
      const std::size_t matIndex = heightIndex * SOBEL_WIDTH + widthIndex;
      const std::size_t upPixIndex = (heightIndex - 1) * SOBEL_WIDTH + widthIndex;
      const std::size_t lowPixIndex = (heightIndex + 1) * SOBEL_WIDTH + widthIndex;
      const std::size_t rightPixIndex = heightIndex * SOBEL_WIDTH + widthIndex + 1;
      const std::size_t leftPixIndex = heightIndex * SOBEL_WIDTH + widthIndex - 1;
       const std::size_t upperRightPixIndex = (heightIndex - 1) * SOBEL_WIDTH + widthIndex + 1;
      const std::size_t upperLeftPixIndex = (heightIndex - 1) * SOBEL_WIDTH + widthIndex - 1;
      const std::size_t lowerRightPixIndex = (heightIndex + 1) * SOBEL_WIDTH + widthIndex + 1;
      const std::size_t lowerLeftPixIndex = (heightIndex + 1) * SOBEL_WIDTH + widthIndex - 1;

      if ((directionMat[matIndex] >= 0.f && directionMat[matIndex] <= 22.5f)
          || (directionMat[matIndex] >= 157.5f && directionMat[matIndex] <= 202.5f)
          || (directionMat[matIndex] >= 337.5f && directionMat[matIndex] <= 360.f)) {
        if (sobel.getSobelMat()[rightPixIndex] > sobel.getSobelMat()[matIndex]
            || sobel.getSobelMat()[leftPixIndex] > sobel.getSobelMat()[matIndex])
          res[matIndex] = 0.f;
      } else if ((directionMat[matIndex] > 22.5f && directionMat[matIndex] <= 67.5f)
                 || (directionMat[matIndex] > 202.5f && directionMat[matIndex] <= 247.5f)) {
        if (sobel.getSobelMat()[lowerRightPixIndex] > sobel.getSobelMat()[matIndex]
            || sobel.getSobelMat()[upperLeftPixIndex] > sobel.getSobelMat()[matIndex])
          res[matIndex] = 0.f;
      } else if ((directionMat[matIndex] > 67.5f && directionMat[matIndex] <= 112.5f)
                 || (directionMat[matIndex] > 247.5f && directionMat[matIndex] <= 292.5f)) {
        if (sobel.getSobelMat()[upPixIndex] > sobel.getSobelMat()[matIndex]
            || sobel.getSobelMat()[lowPixIndex] > sobel.getSobelMat()[matIndex])
          res[matIndex] = 0.f;
      } else if ((directionMat[matIndex] > 112.5f && directionMat[matIndex] <= 157.5f)
                 || (directionMat[matIndex] > 292.5f && directionMat[matIndex] <= 337.5f)) {
        if (sobel.getSobelMat()[upperRightPixIndex] > sobel.getSobelMat()[matIndex]
            || sobel.getSobelMat()[lowerLeftPixIndex] > sobel.getSobelMat()[matIndex])
          res[matIndex] = 0.f;
      }
    }
  }

   /***********************OUR CODE START***********************************/

    //Ends the timer and prints the time it took the program to execute the nested for loop.
    const auto endTime = std::chrono::system_clock::now();
    std::cout << "Computing nested for loop duration: "
            << std::chrono::duration_cast<std::chrono::duration<float>>(endTime - startTime).count()
            << " seconds." << std::endl;

    /***********************OUR CODE END***********************************/
  
  return threshold<ARCV_THRESH_TYPE_HYSTERESIS_AUTO>(res);

}

 /***********************OUR CODE START***********************************/


/************************ applyDetectorHelperGPU *******************************
*     | Function: applyDetectorHehlperGPU<ARC_DETECTOR_TYPE_CANNY>( SOBEL_WIDTH,
      |           SOBEL_HEIGHT, directinoMat, resMat, sobelMat )
*     |
*     | Purpose: Do the edge detection algorithm with a parallel process, using
*     |          thread index calucations based on the SOBEL_HEIGHT.
*     |
*     | param SOBEL_WIDTH The width of the sobel matrix.
*     | param SOBEL_HEIGHT The height of the sobel matrix.
*     | param SOBEL_WIDTH_LOOP The width of the sobel matrix modified to be used
*     |                        in a loop.
*     | param directionMat An array that contains the direction of the sobel 
*     |                    component gradient.
*     | param resMAt An array that contains the resolution of the sobel matrix.
*     | param sobelMat An array that contains the intervals that specify whether
*     |                the resolution needs to be modified.
*     |
*     | return None.
*     |
*************************************************************************/
__global__ void applyDetectorHelperGPU(const int SOBEL_WIDTH, const int SOBEL_HEIGHT, const int SOBEL_WIDTH_LOOP,
                                        const float* directionMat, float* resMat, const float* sobelMat)
{
        //Generates akl possible height indices that the sobel contains.
        size_t heightIndex = blockIdx.x * blockDim.x + threadIdx.x;

        //Sets width to its default width, 1.

        size_t widthIndex = 1;

        int sobelWidth = SOBEL_WIDTH;


        //If the threads current height index is within the sobel height parameters then,
        if( heightIndex < SOBEL_HEIGHT )
        {

        //For every width index of the height we are currently in ( row ) perfom the edge
        //detection algorithm.

        for( ; widthIndex < SOBEL_WIDTH_LOOP; widthIndex++ )
           {
                const float dmMat = directionMat[ heightIndex * sobelWidth + widthIndex ];
                float * resPtr = &( resMat[ heightIndex * sobelWidth + widthIndex ] );
                const float sobelMatRight = sobelMat[ heightIndex * sobelWidth + widthIndex + 1 ];
                const float sobelMatIndex = sobelMat[ heightIndex * sobelWidth + widthIndex ];
                const float sobelMatLeft = sobelMat[ heightIndex * sobelWidth + widthIndex - 1 ];
                const float sobelMatRightL = sobelMat[ ( heightIndex+1 ) * sobelWidth + widthIndex + 1 ];
                const float sobelMatLeftL = sobelMat[ ( heightIndex+1 ) * sobelWidth + widthIndex - 1 ];
                const float sobelMatRightU = sobelMat[ ( heightIndex - 1 ) * sobelWidth + widthIndex + 1 ];
                const float sobelMatLeftU = sobelMat [( heightIndex - 1 ) * sobelWidth + widthIndex - 1 ];
                const float sobelMatUPix = sobelMat [ ( heightIndex - 1 ) * sobelWidth + widthIndex ];
                const float sobelMatLPix = sobelMat [ ( heightIndex + 1 ) * sobelWidth + widthIndex ];

                // A massive if statment that checks whether the intensity of the pixel needs to be modified.

                if ( ( ( ( dmMat >= 0.f && dmMat <= 22.5f ) || ( dmMat >= 157.5f && dmMat <= 202.5f ) || ( dmMat >= 337.5f && dmMat <= 360.f ) ) 
                   && ( sobelMatRight > sobelMatIndex || sobelMatLeft > sobelMatIndex ) ) || ( (( dmMat > 22.5f && dmMat <= 67.5f ) || ( dmMat > 202.5f && dmMat <= 247.5f) 
                   ) && (sobelMatRightL > sobelMatIndex || sobelMatLeftU > sobelMatIndex ) ) || ( ( ( dmMat > 67.5f && dmMat <= 112.5f ) || 
                   ( dmMat > 247.5f && dmMat <= 292.5f ) ) && ( ( sobelMatUPix > sobelMatIndex ) || ( sobelMatLPix > sobelMatIndex ) ) )||
                   ( ( ( dmMat > 112.5f && dmMat <= 157.5f ) || ( dmMat > 292.5f && dmMat <= 337.5f ) ) && ( ( sobelMatRightU > sobelMatIndex ) 
                   || (sobelMatLeftL > sobelMatIndex))))
                   
                   {
                               *(resPtr) = 0.f;
                   }

           }

        }

}
 /***********************OUR CODE END***********************************/

/************************ applyDetectorGPU *******************************
*     | Function: applyDetectorGPU<ARC_DETECTOR_TYPE_CANNY>( mat )
*     |
*     | Purpose: Do the edge detection algorithm with a parallel process,
*     |          in this case we are using CUDA.
*     |
*     | param mat: A matrix of and image.
*     |
*     | return The resolution Matrix that is templated.
*     |
*************************************************************************/
template<>
Matrix<> applyDetectorGPU<ARCV_DETECTOR_TYPE_CANNY>(const Matrix<>& mat) {
  const Sobel sobel(applyFilter<ARCV_FILTER_TYPE_GAUSSIAN_BLUR>(changeColorspace<ARCV_COLORSPACE_GRAY>(mat)));
  const Matrix<> directionMat = sobel.computeGradientDirection();
  Matrix<> res = sobel.getSobelMat(); //Resolution of the sobel.

 /***********************OUR CODE START***********************************/

  //Calculates the sobel width of the sobel matrix.
  const int SOBEL_WIDTH = sobel.getSobelMat().getWidth();

  const int SOBEL_WIDTH_LOOP = SOBEL_WIDTH - 1;
  
  //Calculates the sobel height of the sobel matrix.
  const int SOBEL_HEIGHT = sobel.getSobelMat().getHeight() - 1; 
 
  //Converts all matrices into arrays and store them in pointers.
  //Also gets the size of all the matrices.
  const float* dirMat = directionMat.getData().data();
  float* resMat = res.getData().data();
  const float* sobelMat = sobel.getSobelMat().getData().data();

  //Also gets the size of all the arrays that correspond to the matrices.   
  int resMatSize = res.getData().size();
  int matSize = directionMat.getData().size();
  int sobelSize = sobel.getSobelMat().getData().size(); 

        //Assigns the number of threads to be the maximum amount.
	long long int numThreads = MAXIMUM_THREADS; 

        
        //Assigns the number of cores to be dependent upon the height
        //of the sobel and splits them appropiately.
	long long int numCores = (SOBEL_HEIGHT/MAXIMUM_THREADS) + 1;

        if ( numCores > MAXIMUM_CORES )
        {  
            puts ( "Your image is too large! Please use another one!" );
  
            exit ( EXIT_FAILURE );
        }

        //Pointers to GPU memory location for arrays.
	float* gpuResMat;
        float * gpuMat;
        float * gpuSobelMat;
      
        //Allocates enough memory to store the arrays in the GPU.
	cudaMalloc(&gpuResMat, resMatSize*sizeof(float));
        cudaMalloc(&gpuMat, matSize* sizeof(float) );
        cudaMalloc(&gpuSobelMat, sobelSize * sizeof(float));

        //Copies all the arrays into the GPU.
	cudaMemcpy(gpuResMat, resMat, resMatSize*sizeof(float), cudaMemcpyHostToDevice);
        cudaMemcpy(gpuMat, dirMat, matSize*sizeof(float), cudaMemcpyHostToDevice);
        cudaMemcpy(gpuSobelMat, sobelMat, sobelSize*sizeof(float), cudaMemcpyHostToDevice);

        //Starts recording the speed of our CUDA method.
        const auto startTime = std::chrono::system_clock::now(); 

        applyDetectorHelperGPU<<<numCores, numThreads>>>(SOBEL_WIDTH, SOBEL_HEIGHT, SOBEL_WIDTH_LOOP, gpuMat, gpuResMat, gpuSobelMat);

        //Stops the timer recording the CUDA method and prints out the time to the screen.
        const auto endTime = std::chrono::system_clock::now();

         std::cout << "Computing thread duration: "
            << std::chrono::duration_cast<std::chrono::duration<float>>(endTime - startTime).count()
            << " seconds." << std::endl;
           
           
        //Copies the modified resolution matrix array into the local memory.
        cudaMemcpy(resMat, gpuResMat, resMatSize * sizeof(float), cudaMemcpyDeviceToHost);

        //Frees all allocated memory.
	cudaFree(&gpuResMat);
        cudaFree(&gpuMat);
        cudaFree(&gpuSobelMat);      	

         /***********************OUR CODE***********************************/
	
	return threshold<ARCV_THRESH_TYPE_HYSTERESIS_AUTO>(res);

}


} // namespace Image

} // namespace Arcv
