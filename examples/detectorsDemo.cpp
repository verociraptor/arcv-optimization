#include <chrono>
#include <iostream>
#include <fstream>

/******OUR CODE STARTS HERE*****************/

#include "../include/ArcV/ArcV.hpp"

#define TAJ_TEST "./assets/taj.png"

#define CAVE_TEST "./assets/cave.png"

#define COSMOS_TEST "./assets/nasa.png"

#define VERSAILLES_TEST "./assets/versailles3.png"

int main() {
 
 //Creates an image matrix for the given image input.
 //Test are the constants defined above.
 Arcv::Matrix<float> mat = Arcv::Image::read( CAVE_TEST );

 const auto startTime = std::chrono::system_clock::now(); //Starts timing the beginning of the
                                                          //normal edge detection function. 
                                                          //NOT PARALLEL.

  Arcv::Matrix<float> cannyMat = Arcv::Image::applyDetector<ARCV_DETECTOR_TYPE_CANNY>(mat);

  const auto endTime = std::chrono::system_clock::now(); //The normal edge detection function
                                                         //has terminated its job, stops the timer.


  //Prints out time to the screen for the normal edge detection process.
  std::cout << "Computing duration Normal Execution: "
            << std::chrono::duration_cast<std::chrono::duration<float>>(endTime - startTime).count()
            << " seconds." << std::endl; 

  
  const auto startTimeGPU = std::chrono::system_clock::now(); //Starts timing the beginning of the
                                                              //CUDA edge detection function.
                                                              //PARALLEL.


  //Calls the CUDA function.
  Arcv::Matrix<float> cannyMatGPU = Arcv::Image::applyDetectorGPU<ARCV_DETECTOR_TYPE_CANNY>(mat);

  const auto endTimeGPU = std::chrono::system_clock::now(); //The parallel edge detection function
                                                            //has terminated its job, stops the timer.

     //Prints out time to screen for the parallel edge detection process.
     std::cout << "Computing duration GPU: "
                 << std::chrono::duration_cast<std::chrono::duration<float>>(endTimeGPU - startTimeGPU).count()
                             << " seconds." << std::endl;

  Arcv::Image::write(cannyMat, "OutputNormal.png"); //Generates the image from the normal edge detection.
  Arcv::Image::write(mat, "Image.png"); //Prints out the Image that the program read.
  Arcv::Image::write(cannyMatGPU, "OutputGPU.png"); //Generates the image from the parallel edge detection.

/*****************OUR CODE ENDS HERE**********************/

  return EXIT_SUCCESS;

}
