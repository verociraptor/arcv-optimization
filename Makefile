NVCC=/usr/local/cuda-7.5/bin/nvcc
#NVCC=/usr/local/cuda-9.1/bin/nvcc


#The label to execute the compilation of the detector tester program.
detectorGPU:  examples/detectorsDemo.cpp
	${NVCC} examples/detectorsDemo.cpp src/ArcV/Math/Matrix.cpp src/ArcV/Processing/ImageColorspace.cpp src/ArcV/Processing/ImageDetector.cu src/ArcV/Processing/ImageFilter.cpp src/ArcV/Processing/ImageFormat.cpp src/ArcV/Processing/ImageThreshold.cpp src/ArcV/Processing/Sobel.cpp -o detectorsDemoTest -std=c++11 -lpng -lglut

